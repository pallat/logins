package main

import "net/http"

func Activate(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("wrong method"))
		return
	}

	if err := db.Model(&user{}).Where("active = ?", r.FormValue("key")).Update("active", "").Error; err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}
