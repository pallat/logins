package main

import (
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
)

const (
	secretKey = "12345678123456781234567812345678"
)

func LoginByEmail(w http.ResponseWriter, r *http.Request) {
	var u user

	if r.Method == "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("wrong method"))
		return
	}

	credential := map[string]string{}
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	err = json.Unmarshal(b, &credential)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if _, ok := credential["email"]; !ok {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("email is empty"))
		return
	}

	if _, ok := credential["password"]; !ok {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("password is empty"))
		return
	}

	bv := []byte(credential["password"])
	hasher := sha1.New()
	hasher.Write(bv)
	password := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	fmt.Println(password)

	if err := db.Last(&u, "email = ? and password = ?", credential["email"], password).Error; err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if u.Active != "" {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("user inactive"))
		return
	}

	auth0 := jwt.New(jwt.GetSigningMethod("HS256"))
	token, err := auth0.SignedString([]byte(secretKey))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, `{"token": "%s"}`, token)
}
