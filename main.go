package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	jwtmiddleware "github.com/auth0/go-jwt-middleware"
	"github.com/codegangsta/negroni"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var (
	db *gorm.DB
)

//https://www.freemysqlhosting.net/account/
//http://www.phpmyadmin.co/#
func init() {
	var err error
	db, err = gorm.Open("mysql", "sql6158987:jAbGuHjuvi@tcp(sql6.freemysqlhosting.net:3306)/sql6158987?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		log.Fatal(err)
	}
	db.AutoMigrate(&user{})
}

func main() {
	r := mux.NewRouter()

	jwtMiddleware := jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return []byte(secretKey), nil
		},
		// When set, the middleware verifies that tokens are signed with the specific signing algorithm
		// If the signing method is not constant the ValidationKeyGetter callback can be used to implement additional checks
		// Important to avoid security issues described here: https://auth0.com/blog/2015/03/31/critical-vulnerabilities-in-json-web-token-libraries/
		SigningMethod: jwt.SigningMethodHS256,
	})

	r.Handle("/signup", negroni.New(
		negroni.Wrap(http.HandlerFunc(SignupByEmail)),
	))
	r.Handle("/login", negroni.New(
		negroni.Wrap(http.HandlerFunc(LoginByEmail)),
	))
	r.Handle("/activate", negroni.New(
		negroni.Wrap(http.HandlerFunc(Activate)),
	))
	r.Handle("/check", negroni.New(
		negroni.Wrap(http.HandlerFunc(healthy)),
	))
	r.Handle("/profile", negroni.New(
		negroni.HandlerFunc(jwtMiddleware.HandlerWithNext),
		negroni.Wrap(http.HandlerFunc(Profile)),
	))
	http.Handle("/", handlers.CORS(handlers.AllowedOrigins([]string{"*"}), handlers.AllowedMethods([]string{"OPTIONS", "GET", "POST"}), handlers.AllowedHeaders([]string{"Authorization"}))(r))

	port := ":1234"
	if os.Getenv("PORT") != "" {
		port = ":" + os.Getenv("PORT")
	}

	fmt.Println("listening", port)

	log.Fatal(http.ListenAndServe(port, nil))
}

func healthy(w http.ResponseWriter, r *http.Request) {
	log.Println("alive....")
}
