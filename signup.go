package main

import (
	"bytes"
	"crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"html/template"
	"io/ioutil"
	"net/http"
	"net/smtp"
	"os"

	"fmt"

	"github.com/jinzhu/gorm"
)

func SignupByEmail(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		w.WriteHeader(http.StatusNotImplemented)
		w.Write([]byte("wrong method"))
		return
	}

	credential := map[string]string{}
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	err = json.Unmarshal(b, &credential)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if _, ok := credential["email"]; !ok {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("email is empty"))
		return
	}

	if _, ok := credential["password"]; !ok {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("password is empty"))
		return
	}

	bv := []byte(credential["password"])
	hasher := sha1.New()
	hasher.Write(bv)
	password := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	fmt.Println(password)

	bkey := make([]byte, 8)
	rand.Read(bkey)
	activate := base64.URLEncoding.EncodeToString(bkey)

	u := user{
		Email:    credential["email"],
		Password: password,
		Active:   activate,
	}

	if err := db.Create(&u).Error; err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = sendEmail(u.Email, u.Active)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		db.Delete(&user{}, "email = ?", u.Email)
		return
	}

	fmt.Fprintf(w, "please check your email")
}

type user struct {
	gorm.Model
	Email     string `sql:"size:255;unique;index" json:"email"`
	Password  string `json:"password"`
	Active    string `json:"-"`
	Fullname  string
	Address   string
	Telephone string
}

func sendEmail(to, key string) error {
	auth := smtp.PlainAuth("", "pallat.dev@gmail.com", "xitgmLwmp", "smtp.gmail.com")
	data := map[string]string{
		"From":    "pallat.dev@gmail.com",
		"To":      to,
		"Subject": "signup",
		"Body": `Hi

Thank you for registering with us.

Please use this link to complete your registration.
` + os.Getenv("activateURL") + `/activate?key=` + key + `

Many thanks
pallat`,
	}

	b := bytes.NewBuffer([]byte{})
	t := template.Must(template.New("letter").Parse(letterTemplate))
	err := t.Execute(b, data)
	if err != nil {
		return err
	}

	err = smtp.SendMail("smtp.gmail.com:25", auth, "pallat.dev@gmail.com", []string{to}, b.Bytes())
	if err != nil {
		return err
	}

	return nil

}

var letterTemplate = `From: {{.From}}
To: {{.To}}
Cc: {{.Cc}}
Subject: {{.Subject}}

{{.Body}}

    Sincerely,
    Pallat
`
