package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func Profile(w http.ResponseWriter, r *http.Request) {
	var u user

	if r.Method == "OPTIONS" {
		return
	}

	if r.Method == "POST" {
		EditProfile(w, r)
		return
	}

	if db.Last(&u, "email = ?", r.FormValue("email")).Error != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(db.Error.Error()))
		return
	}

	b, err := json.Marshal(&u)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(b)
}

func EditProfile(w http.ResponseWriter, r *http.Request) {
	var u user

	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = json.Unmarshal(b, &u)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if err := db.Model(&user{}).Where("email = ?", u.Email).Updates(&u).Error; err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}
